
0.1.3 / 2014-07-07
==================

 * add #off for tab
 * remove deep_linking feature from tab

0.1.2 / 2014-07-03
==================

 * mv contents of scss to root
 * fix readme

0.1.1 / 2014-07-03
==================

 * include normalize by default

0.1.0 / 2014-07-03
==================

 * initial mirror
